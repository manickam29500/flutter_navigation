import 'package:flutter/material.dart';
import 'package:flutter_navigation/bottomNavigator.dart';
import 'package:flutter_navigation/screen1.dart';
import 'package:flutter_navigation/screen3.dart';

class ScreenTwo extends StatelessWidget {
  const ScreenTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              leading: Icon(
                Icons.screen_share_rounded,
              ),
              title: const Text('ScreenOne'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ScreenOne()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.music_note,
              ),
              title: const Text('ScreenTwo'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ScreenTwo()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.location_on,
              ),
              title: const Text('ScreenThree'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ScreenThree()));
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Screen 2'),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context,
                      MaterialPageRoute(builder: (context) => ScreenOne()));
                },
                child: Text(
                  'Back',
                  style: TextStyle(fontSize: 30),
                ),
              ),
            ),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ScreenThree()));
                },
                child: Text(
                  'Next',
                  style: TextStyle(fontSize: 30),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigator(),
    );
  }
}
