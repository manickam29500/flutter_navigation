import 'package:flutter/material.dart';
import 'package:flutter_navigation/bottomNavigator.dart';
import 'package:flutter_navigation/screen2.dart';
import 'package:flutter_navigation/screen3.dart';

class ScreenOne extends StatelessWidget {
  const ScreenOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              leading: Icon(
                Icons.screen_share_rounded,
              ),
              title: const Text('ScreenOne'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ScreenOne()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.music_note,
              ),
              title: const Text('ScreenTwo'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ScreenTwo()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.location_on,
              ),
              title: const Text('ScreenThree'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ScreenThree()));
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Screen 1'),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ScreenTwo()));
              },
              child: Text(
                'Next',
                style: TextStyle(fontSize: 30),
              ),
            ),
          ],
        )
      ),
      bottomNavigationBar: BottomNavigator(),
    );
  }
}
//
// BottomNavigationBar(
// currentIndex: 0,
// onTap: (value) {
// if (value == 0) {
// Navigator.push(
// context, MaterialPageRoute(builder: (context) => ScreenOne()));
// } else if (value == 1) {
// Navigator.push(
// context, MaterialPageRoute(builder: (context) => ScreenTwo()));
// } else {
// Navigator.push(context,
// MaterialPageRoute(builder: (context) => ScreenThree()));
// }
// },
// items: [
// BottomNavigationBarItem(
// icon: Icon(Icons.screen_share_rounded),
// label: 'ScreenOne',
// ),
// BottomNavigationBarItem(
// icon: Icon(Icons.music_note),
// label: 'ScreenTwo',
// ),
// BottomNavigationBarItem(
// icon: Icon(Icons.location_on),
// label: 'ScreenThree',
// ),
// ],
// ),
