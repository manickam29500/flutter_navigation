import 'package:flutter/material.dart';
import 'package:flutter_navigation/screen1.dart';
import 'package:flutter_navigation/screen2.dart';
import 'package:flutter_navigation/screen3.dart';

class BottomNavigator extends StatefulWidget {
  const BottomNavigator({Key? key}) : super(key: key);

  @override
  State<BottomNavigator> createState() => _BottomNavigatorState();
}

class _BottomNavigatorState extends State<BottomNavigator> {
  @override
  int _selectedIndex = 0;

  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      unselectedItemColor: Colors.black,
      selectedItemColor: Colors.black,
      items: [
        BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
          ),
          label: 'screen1',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.time_to_leave,
          ),
          label: 'screen2',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.account_circle,
          ),
          label: 'screen3',
        ),
      ],
      onTap: (index) {
        setState(() {
          _selectedIndex = index;
        });
        if (_selectedIndex == 0) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ScreenOne()));
        } else if (_selectedIndex == 1) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ScreenTwo()));
        } else if (_selectedIndex == 2) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ScreenThree()));
        }
      },
    );
  }
}
